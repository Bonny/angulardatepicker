/* global angular, PAGESCOPE, dictionary, appProperties, Locale, app */

//Array.prototype.contains = function contains(obj) {
//   for (var i = 0; i < this.length; i++) {
//      if (this[i] === obj) {
//         return true;
//      }
//   }
//   return false;
//};

var ColorUtils = (function () {
   return {
      hexToRgb: function (hex) {
         // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
         var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
         hex = hex.replace(shorthandRegex, function (m, r, g, b) {
            return r + r + g + g + b + b;
         });

         var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
         return result ? [
            parseInt(result[1], 16),
            parseInt(result[2], 16),
            parseInt(result[3], 16)
         ] : [0, 0, 0];
      },
      isLight: function (hex) {
         var rgb = this.hexToRgb(hex);
         return (
                 0.213 * rgb[0] +
                 0.715 * rgb[1] +
                 0.072 * rgb[2] >
                 255 / 2
                 );
      }
   };
})();

function transformRequest(obj) {
   var str = [];
   for (var p in obj)
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
   return str.join("&");
}

function  replaceAll(find, replace, str) {
   return str.replace(new RegExp(find, 'g'), replace);
}

function  extend(destination, source) {
   for (var property in source) {
      destination[property] = source[property];
   }
   return destination;
}

function normalizeTime(t) {

   if (t > 0 && t < 10)
      t = "0" + t.toString();
   else if (t === 0)
      t = "00";

   return t.toString();
}

function count(value) {
   return (value && angular.isDefined(value) ? value.length : 0);
}

function format(obj, sep) {
   if (!sep)
      sep = ",";
   var dim = count(obj), c = 0, ret = "";
   for (var i in obj) {

      ret += (i + "=" + obj[i]) + ",";
      if (c < dim - 1) {
         //ret += sep;
      }
      c++;
   }
   return ret;
}

function isBool(b) {
   return (/^true$/i).test(b);
}

function formatXml(xml) {
   var formatted = '';
   var reg = /(>)(<)(\/*)/g;
   xml = xml.replace(reg, '$1\r\n$2$3');
   var pad = 0;
   jQuery.each(xml.split('\r\n'), function (index, node) {
      var indent = 0;
      if (node.match(/.+<\/\w[^>]*>$/)) {
         indent = 0;
      } else if (node.match(/^<\/\w/)) {
         if (pad != 0) {
            pad -= 1;
         }
      } else if (node.match(/^<\w[^>]*[^\/]>.*$/)) {
         indent = 1;
      } else {
         indent = 0;
      }

      var padding = '';
      for (var i = 0; i < pad; i++) {
         padding += '  ';
      }

      formatted += padding + node + '\r\n';
      pad += indent;
   });

   return formatted;
}

if (typeof String.prototype.startsWith != 'function') {
   String.prototype.startsWith = function (prefix) {
      return this.slice(0, prefix.length) == prefix;
   };
}

if (typeof String.prototype.endsWith != 'function') {
   String.prototype.endsWith = function (suffix) {
      return this.slice(-suffix.length) == suffix;
   };
}

/**
 * Sistema la stringa contenuta in TO e CC.
 * Rimuove, se presente, l'ultima virgola e gli spazi bianchi
 * 
 * @param input
 */
function adjustCompose(input) {

   if (input == undefined) {
      return undefined;
   } else if (input == null) {
      return null;
   }

   var tmp = input.trim();
   if (tmp.endsWith(",")) {
      tmp = tmp.slice(0, -1);
   }
   return tmp;
}


function nl2br(str, is_xhtml) {
   // *     example 1: nl2br('Kevin\nvan\nZonneveld');
   // *     returns 1: 'Kevin<br />\nvan<br />\nZonneveld'
   // *     example 2: nl2br("\nOne\nTwo\n\nThree\n", false);
   // *     returns 2: '<br>\nOne<br>\nTwo<br>\n<br>\nThree<br>\n'
   // *     example 3: nl2br("\nOne\nTwo\n\nThree\n", true);
   // *     returns 3: '<br />\nOne<br />\nTwo<br />\n<br />\nThree<br />\n'
   var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display
   return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function () {

   $(window).bind("load resize", function () {
      var topOffset = 50;
      var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
      if (width < 768) {
         $('div.navbar-collapse').addClass('collapse');
         topOffset = 100; // 2-row-menu
      } else {
         $('div.navbar-collapse').removeClass('collapse');
      }

      var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
      height = height - topOffset;
      if (height < 1)
         height = 1;
      if (height > topOffset) {
         $("#page-wrapper").css("min-height", (height) + "px");
      }
   });

   var url = window.location;
   var element = $('ul.nav a').filter(function () {
      return this.href == url;
   }).addClass('active').parent().parent().addClass('in').parent();
   if (element.is('li')) {
      element.addClass('active');
   }

});

function translateScope(input) {

   if (input == PAGESCOPE.INBOX) {
      return dictionary['inbox'];
   } else if (input == PAGESCOPE.OUTBOX) {
      return dictionary['sent'];
   } else if (input == PAGESCOPE.TRASH) {
      return dictionary['trash'];
   } else if (input == PAGESCOPE.UNMATCHBOX) {
      return dictionary['unmatched'];
   } else if (input == PAGESCOPE.DRAFTBOX) {
      return dictionary['draft'];
   } else if (input == PAGESCOPE.TRASHBOX) {
      return dictionary['trash'];
   } else if (input == PAGESCOPE.REPORT) {
      return dictionary['report'];
   }
}

function getScopeIcon(input) {
   if (input == PAGESCOPE.INBOX) {
      return 'fa-download';
   } else if (input == PAGESCOPE.OUTBOX) {
      return 'fa-upload';
   } else if (input == PAGESCOPE.TRASH) {
      return 'fa-trash-o';
   } else if (input == PAGESCOPE.UNMATCHBOX) {
      return 'fa-sitemap';
   } else if (input == PAGESCOPE.DRAFTBOX) {
      return 'fa-tasks';
   } else if (input == PAGESCOPE.TRASHBOX) {
      return 'fa-trash-o';
   } else if (input == PAGESCOPE.REPORT) {
      return 'fa-line-chart';
   }
}

function getScopePanel(input) {
   if (input == PAGESCOPE.INBOX) {
      return 'primary';
   } else if (input == PAGESCOPE.OUTBOX) {
      return 'green';
   } else if (input == PAGESCOPE.UNMATCHBOX) {
      return 'danger';
   } else if (input == PAGESCOPE.DRAFTBOX) {
      return 'yellow';
   } else 
      return 'default';
}

function wsp_view_msg(uuid, id_notifica) {
   angular.element(document.querySelector('[ng-controller]')).injector().get('MessageService').load({uuid: uuid});
   if (parseInt(id_notifica) > 0) {
      angular.element(document.getElementById("top-menu")).scope().clearNotice({id: id_notifica}, false);
   }
}

function init() {
   document.getElementById("app-version").innerHTML = "v." + appProperties.relVersion;
   document.getElementById("app-locale").innerHTML = Locale;
}

var DateUtils = (function () {
   return {
      humanTranslate: function (time) {

         var date = new Date(time),
                 diff = (((new Date()).getTime() - date.getTime()) / 1000),
                 day_diff = Math.floor(diff / 86400);
         var year = date.getFullYear(),
                 month = date.getMonth() + 1,
                 day = date.getDate();

         if (isNaN(day_diff) || day_diff < 0 || day_diff >= 31)
            return (
                    ((day < 10) ? '0' + day.toString() : day.toString()) + '/'
                    + ((month < 10) ? '0' + month.toString() : month.toString()) + '/'
                    + year.toString()
                    );

         return ((day_diff == 0 && ((diff < 60 && "Adesso")
                 || (diff < 120 && "1 minuto fa")
                 || (diff < 3600 && Math.floor(diff / 60) + " minuti fa")
                 || (diff < 7200 && "1 hour ago")
                 || (diff < 86400 && Math.floor(diff / 3600) + " ore fa")
                 ))
                 || (day_diff == 1 && "Ieri")
                 || (day_diff < 7 && day_diff + " giorni fa")
                 || (day_diff < 31 && Math.ceil(day_diff / 7) + " settimane fa")
                 );

      }
   };
})();


app.filter("purger", function () {
   return function (input) {
      return input ? input.replace(/[\\$\\^]/g, "") : "";
   };
});

function setStyleToLabels(labels) {

   if (labels) {

      for (var i = 0; i < count(labels); i++) {

         var color = "#" + labels[i].color;
         var textcolor = (ColorUtils.isLight(color) ? "#000" : "#FFF");

         labels[i].style = {
            "color": textcolor,
            "background-color": color,
            "padding": "2px",
            "border": "1px solid " + ((color.toUpperCase() === "#FFF" || color.toUpperCase() === "#FFFFFF") ? "#6b6b6b" : color)
         };

      }

   }

   return labels;
}

app.controller("appTitleCtrl", ["$scope", "appProperties",
   function ($scope, appProperties) {
      $scope.title = appProperties.appName + " v." + appProperties.relVersion;
   }
]);

app.filter('reverse', function () {
   return function (items) {
      return items.slice().reverse();
   };
});

var RegExpValidator = (function () {

   var mail = "[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";

   //var regExpMail = new RegExp("^(([A-Za-z0-9]*(" + mail + "))|("+mail+"))$");

   var regExpMail;

   // v1
   //regExpMail = new RegExp(/^(?:([\w\s]+)\s*<(\w+)([\-+.][\w]+)*@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6}>|(\w+)([\-+.][\w]+)*@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6})$/);
   // v2
   regExpMail = new RegExp(/^(?:([@._A-Za-z0-9-]+)\s*<(\w+)([\-+.][\w]+)*@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6}>|(\w+)([\-+.][\w]+)*@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6})$/);

   return {
      isMailAddress: function (str) {
         return regExpMail.test(str);
      },
      isMailOnly: function (str) {
         return new RegExp("^" + mail + "$").test(str);
      },
      isMailOnlyWithBracket: function (str) {
         return new RegExp("^<" + mail + ">$").test(str);
      }
   };

})();