/* global app, dictionary, Evt */

app.controller('demoCtrl', ['$scope', '$log',
    function ($scope, $log) {

        var now = new Date();
        var yesterday = new Date();
        yesterday.setDate(now.getDate() - 1);

        $scope.quick = {
            up: false,
            search: "",
            searchTmp: "",
            time: false,
            datepickerFrom: yesterday.getTime(),
            datepickerTo: now.getTime(),
            datepickerError: false,
            isNoFastSearch: false,
            searchScope: []
        };

        var _CODE = "MESSAGES";

        $scope.$on(Evt.DPR_CHANGE_FROM, function (event, code, from) {
            if (code === _CODE) {
                $log.debug("BodyPageController event=" + Evt.DPR_CHANGE_FROM + ", code=" + code + ", from=" + from);
                $scope.quick.datepickerFrom = from;
            }
        });

        $scope.$on(Evt.DPR_CHANGE_TO, function (event, code, to) {
            if (code === _CODE) {
                $log.debug("BodyPageController event=" + Evt.DPR_CHANGE_TO + ", code=" + code + ", to=" + to);
                $scope.quick.datepickerTo = to;
            }
        });

        $scope.$on(Evt.DPR_ERROR, function (event, code, error) {
            if (code === _CODE) {
                $log.debug("BodyPageController event=" + Evt.DPR_ERROR + ", code=" + code + ", error=" + error);
                $scope.quick.datepickerError = error;
            }
        });

        $scope.datepicker = new Date().getTime();
    }
]);
