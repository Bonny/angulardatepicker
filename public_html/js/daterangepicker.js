
/* global app, dictionary, Evt, moment */

app.directive('daterangepicker', [function () {
        return {
            restrict: 'E',
            scope: {
                code: '='
            },
            templateUrl: 'daterangepicker.html',
            controller: ['$scope', '$log', 'Months', 'Days', '$rootScope', function ($scope, $log, Months, Days, $rootScope) {

                    var dayText = Days;

                    $scope.daysFrom = [];
                    $scope.daysTo = [];

                    $scope.years = [];
                    $scope.months = [];

                    $scope.datepickerError = false;

                    for (var i = 0; i < count(Months); i++) {
                        $scope.months.push({key: i, value: Months[i]});
                    }

                    for (var y = 2010; y <= (new Date().getFullYear()); y++) {
                        $scope.years.push({
                            key: y,
                            value: y
                        });
                    }

                    $scope.reconfigureFrom = function (date) {

                        if (!date)
                            date = 1;

                        $scope.dayFrom = date;
                        var start = moment({year: $scope.yearFrom, month: $scope.monthFrom, date: date});
                        $scope.daysFrom = [];

                        for (var i = 0; i < start.daysInMonth(); i++) {

                            var date = moment({year: $scope.yearFrom, month: $scope.monthFrom, date: i + 1});
                            var day = date.day();

                            $scope.daysFrom.push({
                                key: i + 1,
                                value: dayText[day] + " " + (i + 1)
                            });
                        }


                    };

                    $scope.reconfigureTo = function (date) {

                        if (!date)
                            date = 1;

                        $scope.dayTo = date;
                        var start = moment({year: $scope.yearTo, month: $scope.monthTo, date: date});
                        $scope.daysTo = [];

                        for (var i = 0; i < start.daysInMonth(); i++) {

                            var date = moment({year: $scope.yearTo, month: $scope.monthTo, date: i + 1});
                            var day = date.day();

                            $scope.daysTo.push({
                                key: i + 1,
                                value: dayText[day] + " " + (i + 1)
                            });
                        }

                    };

                    function check() {
                        $scope.datepickerError = $scope.datepickerFrom > $scope.datepickerTo;
                        //$log.debug("daterangepicker");
                        //$log.debug({datepickerFrom: $scope.datepickerFrom, datepickerTo: $scope.datepickerTo, datepickerError: $scope.datepickerError});
                        $rootScope.$broadcast(Evt.DPR_ERROR, $scope.code, $scope.datepickerError);
                    }

                    $scope.changeFrom = function () {
                        $scope.reconfigureFrom();
                        $scope.rebindFrom();
                    };

                    $scope.changeTo = function () {
                        $scope.reconfigureTo();
                        $scope.rebindTo();
                    };

                    $scope.rebindFrom = function () {
                        var m = moment({year: $scope.yearFrom, month: $scope.monthFrom, date: $scope.dayFrom});
                        $scope.datepickerFrom = m.valueOf();
                        check();
                        $rootScope.$broadcast(Evt.DPR_CHANGE_FROM, $scope.code, $scope.datepickerFrom);
                    };

                    $scope.rebindTo = function () {
                        var m = moment({year: $scope.yearTo, month: $scope.monthTo, date: $scope.dayTo, hour: 23, minute: 59, second: 59});
                        $scope.datepickerTo = m.valueOf();
                        check();
                        $rootScope.$broadcast(Evt.DPR_CHANGE_TO, $scope.code, $scope.datepickerTo);
                    };

                    $scope.init = function () {

                        $log.info("init");

                        var now = moment();
                        var yesterday = moment().set("date", now.get("date") - 1);

                        $log.info("now = " + now.format("D/M/YYYY"));
                        $log.info("yesterday = " + yesterday.format("D/M/YYYY"));

                        $scope.dayFrom = yesterday.get("date");
                        $scope.monthFrom = yesterday.get("month");
                        $scope.yearFrom = yesterday.get("year");

                        $scope.dayTo = now.get("date");
                        $scope.monthTo = now.get("month");
                        $scope.yearTo = now.get("year");

                        $scope.reconfigureFrom($scope.dayFrom);
                        $scope.reconfigureTo($scope.dayTo);

                    };

                    $scope.$on(Evt.DPR_RESET, function (event, code) {
                        if (code === $scope.code) {
                            $scope.init();
                        }
                    });

                    $scope.init();
                }],
            link: function (scope, element, attrs) {

//            scope.$parent.$watch('datepicker', function (newVal, oldVal) {
//
//               if (newVal) {
//
//                  var date = new Date(newVal);
//
//                  scope.day = date.getDate();
//                  scope.month = date.getMonth();
//                  scope.year = date.getFullYear();
//
//                  scope.reconfigure();
//               }
//            });

            }
        };
    }
]);


